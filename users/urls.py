from django.urls import path
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token
from users.api.views import UserCreateView

urlpatterns = [
    path('create-user', UserCreateView.as_view(), name='create_user'),
    path('get-token/', obtain_jwt_token, name='get_token'),
    path('refresh-token/', refresh_jwt_token, name='refresh_token'),
]
