from django.test import TestCase
from django.urls import reverse_lazy
from rest_framework import status


class ArticleTestCase(TestCase):

    def test_api_creates_user_correctly(self):
        """API properly creates a new model instance"""
        post_response = self.client.post(reverse_lazy('create_user'),
                                         {'username': 'Zellos', 'password': 'test123', 'email': 'test@test.com'})
        self.assertEqual(post_response.status_code, status.HTTP_201_CREATED)
