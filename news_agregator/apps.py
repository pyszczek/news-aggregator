from django.apps import AppConfig


class NewsAgregatorConfig(AppConfig):
    name = 'news_agregator'
