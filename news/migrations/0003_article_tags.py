# Generated by Django 2.2.3 on 2019-07-18 18:37

import taggit.managers
from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ('taggit', '0003_taggeditem_add_unique_index'),
        ('news', '0002_auto_20190715_2116'),
    ]

    operations = [
        migrations.AddField(
            model_name='article',
            name='tags',
            field=taggit.managers.TaggableManager(help_text='A comma-separated list of tags.',
                                                  through='taggit.TaggedItem', to='taggit.Tag', verbose_name='Tags'),
        ),
    ]
