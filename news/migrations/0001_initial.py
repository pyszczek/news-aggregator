# Generated by Django 2.2.3 on 2019-07-12 11:11

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Article',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(help_text='Article title', max_length=512)),
                ('content', models.TextField(help_text='Article content')),
                ('author', models.CharField(blank=True, help_text='Article author', max_length=256)),
                ('published_date', models.DateField(help_text='Article published on')),
            ],
            options={
                'ordering': ['-published_date'],
            },
        ),
    ]
