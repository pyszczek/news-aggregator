from django_filters.rest_framework import DjangoFilterBackend
from news.api.filters import ArticleFilterSet
from news.api.serializers import ArticleSerializer
from news.models import Article
from rest_framework import generics, permissions


class ArticleListCreateAPIView(generics.ListCreateAPIView):
    serializer_class = ArticleSerializer
    queryset = Article.objects.all()
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    filter_backends = (DjangoFilterBackend,)
    filter_class = ArticleFilterSet


class ArticleRetrieveAPIView(generics.RetrieveAPIView):
    serializer_class = ArticleSerializer
    queryset = Article.objects.all()
