from news.models import Article
from rest_framework import serializers


class ArticleSerializer(serializers.ModelSerializer):
    tags = serializers.SerializerMethodField(method_name='get_list_of_tags')

    class Meta:
        model = Article
        fields = ('title', 'content', 'author', 'published_date', 'tags')

    def get_list_of_tags(self, obj):
        tags_list = list(obj.tags.names())
        return sorted(tags_list)
