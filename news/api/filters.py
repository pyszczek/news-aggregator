from django_filters import rest_framework as filters
from news.models import Article


class TagsFilter(filters.CharFilter):
    def filter(self, queryset, value):
        if value:
            tags = [tag.strip() for tag in value.split(',')]
            queryset = queryset.filter(tags__name__in=tags).distinct()

        return queryset


class ArticleFilterSet(filters.FilterSet):
    tags = TagsFilter()

    class Meta:
        model = Article
        fields = ['tags']
