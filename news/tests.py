from django.test import TestCase
from django.urls import reverse_lazy
from news import factories
from news.helpers import prepare_tags
from news.stopwords import stopwords
from rest_framework import status


class HelpersTestCase(TestCase):

    def test_prepare_tags_works_as_expected(self):
        """
        Test to check if prepare_tags function behaves as expected when filtering out stopwords.
        Feeds the function a string of words based only on stopwords, with expected return result being an empty list.
        """
        test_string = ''
        for word in stopwords:
            test_string += word + ' '
        result = prepare_tags(test_string)
        self.assertEqual(result, [])


class ArticleTestCase(TestCase):

    def test_api_articles_list(self):
        """API correctly returns model content"""
        factories.ArticleFactory.create()
        response = self.client.get(reverse_lazy('articles'))
        self.assertEqual(response.json(), [{'title': 'Test article', 'content': 'This is some test content.',
                                            'author': 'zellos', 'published_date': '2019-07-16',
                                            'tags': ['content', 'test']}])
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_stopwords_are_removed_correctly(self):
        """Tagging function properly discards stopwords when counting most used words"""
        factories.StopwordsTestFactory.create()
        response = self.client.get(reverse_lazy('articles'))
        self.assertEqual(response.json(), [{'title': 'Stopwords Test',
                                            'content': 'Stopwords: this them, us we are one, two but three and four, '
                                                       'although this is something, and definitely not an article.',
                                            'author': 'zellos', 'published_date': '2019-07-20',
                                            'tags': ['article', 'stopwords']}])
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_api_creates_correctly(self):
        """API properly creates a new model instance"""
        test_user = factories.UserFactory.create()
        self.client.force_login(test_user)
        post_response = self.client.post(reverse_lazy('articles'),
                                         {'title': 'Test Article 2', 'content': 'Test content 2.', 'author': 'zellos',
                                          'published_date': '2019-07-15'})
        self.assertEqual(post_response.status_code, status.HTTP_201_CREATED)
        self.client.logout()

    def test_api_article_create_with_non_authorized_user(self):
        """API refuses to create a new article when user is not logged in"""
        post_response = self.client.post(reverse_lazy('articles'),
                                         {'title': 'Test Article 3', 'content': 'Test content 3.', 'author': 'zellos',
                                          'published_date': '2019-07-16'})
        self.assertEqual(post_response.status_code, status.HTTP_401_UNAUTHORIZED)
