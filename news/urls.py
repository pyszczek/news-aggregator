from django.urls import path
from news.api.views import ArticleListCreateAPIView, ArticleRetrieveAPIView

urlpatterns = [
    path('articles/', ArticleListCreateAPIView.as_view(), name='articles'),
    path('get-article/<pk>', ArticleRetrieveAPIView.as_view(), name='get-article'),
]
