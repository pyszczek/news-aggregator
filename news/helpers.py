from collections import Counter

import nltk
from news.stopwords import stopwords

nltk.download('punkt')


def prepare_tags(article_content):
    words = nltk.word_tokenize(article_content, language='english')

    # Clean the list of all English stopwords and punctuation
    cleaned_list = [word.lower() for word in words if word.lower() not in stopwords and word.isalpha()]

    count = Counter(cleaned_list)

    # "Sorts" the dict by values, returning tuples containing top 20 words
    sorted_by_value = sorted(count.items(), key=lambda kv: kv[1], reverse=True)[:20]

    words_list = [element[0] for element in sorted_by_value]

    return words_list
