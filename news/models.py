from django.db import models
from taggit.managers import TaggableManager


class Article(models.Model):
    title = models.CharField(max_length=512, help_text='Article title')
    content = models.TextField(help_text='Article content')
    author = models.CharField(max_length=256, help_text='Article author', blank=True)
    published_date = models.DateField(help_text='Article published on')
    tags = TaggableManager()

    class Meta:
        ordering = ['-published_date']
        unique_together = ('title', 'author')

    def __str__(self):
        return self.title
