import factory
from django.contrib.auth.models import User
from news.models import Article


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User

    username = 'testuser'
    password = '12345'
    is_staff = False


class ArticleFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Article

    title = 'Test article'
    content = 'This is some test content.'
    author = 'zellos'
    published_date = '2019-07-16'


class StopwordsTestFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Article

    title = 'Stopwords Test'
    content = 'Stopwords: this them, us we are one, two but three and four, although this is something, ' \
              'and definitely not an article.'
    author = 'zellos'
    published_date = '2019-07-20'
