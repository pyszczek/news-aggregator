from django.db.models.signals import post_save
from django.dispatch import receiver
from news.helpers import prepare_tags
from news.models import Article


@receiver(post_save, sender=Article)
def tag_article(sender, instance, created, **kwargs):
    if created:
        instance.refresh_from_db()
        tags = prepare_tags(instance.content)
        for tag in tags:
            instance.tags.add(tag)
